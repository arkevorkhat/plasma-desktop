# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2015.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-17 00:51+0000\n"
"PO-Revision-Date: 2022-12-22 10:31+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/main.qml:27
#, kde-format
msgid ""
"The system must be restarted before changes to the middle-click paste "
"setting can take effect."
msgstr "يجب إعادة تشغيل النظام لتأخذ تغييرات اللصق بالزر الأوسط مفعولها."

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Restart"
msgstr "أعد التّشغيل"

#: package/contents/ui/main.qml:50
#, kde-format
msgid "Visual behavior:"
msgstr "السلوك المرئي:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: package/contents/ui/main.qml:51 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "عرض تلميحات إعلامية عند تحويم الفأرة"

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: package/contents/ui/main.qml:62 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "عرض مؤشر مرئي عند تغيير الحالة"

#: package/contents/ui/main.qml:80
#, kde-format
msgid "Animation speed:"
msgstr "سرعة التحريك:"

#: package/contents/ui/main.qml:103
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "بطيء"

#: package/contents/ui/main.qml:109
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "فوري"

#: package/contents/ui/main.qml:124
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "النقر على الملفات و المجلدات:"

#: package/contents/ui/main.qml:125
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "يفتحها"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "تحديد بواسطة النقر على علامة التحديد في العنصر"

#: package/contents/ui/main.qml:147
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "يحددها"

#: package/contents/ui/main.qml:160
#, kde-format
msgid "Open by double-clicking instead"
msgstr "فتح بواسطة النقر المزدوج  بدلا عن ذلك"

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr "النقر في شريط التمرير:"

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr "يمرر صفحة واحدة للأعلى أو الأسفل"

#: package/contents/ui/main.qml:188
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr "انقر بزر الفأرة الأوسط للتمرير إلى الموقع الذي نقر فوقه"

#: package/contents/ui/main.qml:198
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr "يمرر إلى المكان المنقور عليه"

#: package/contents/ui/main.qml:217
#, kde-format
msgid "Middle Click:"
msgstr "النّقر بالوسط:"

#: package/contents/ui/main.qml:219
#, kde-format
msgid "Paste selected text"
msgstr "يلصق النص المحدد"

#: package/contents/ui/main.qml:236
#, kde-format
msgid "Touch Mode:"
msgstr "وضع اللمس:"

#: package/contents/ui/main.qml:238
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr "يفعله عند الحاجة آليا"

#: package/contents/ui/main.qml:238 package/contents/ui/main.qml:267
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr "غير ممكن"

#: package/contents/ui/main.qml:250
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""
"سينشط وضع اللمس تلقائيًا عندما يكتشف النظام شاشة تعمل باللمس ولكن لا يوجد "
"فأرة أو لوحة لمس. على سبيل المثال: عند قلب لوحة مفاتيح الحاسوب المحمول "
"القابل للتحويل أو فصلها."

#: package/contents/ui/main.qml:255
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr "مفعّل دائمًا"

#: package/contents/ui/main.qml:280
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""
"في وضع اللمس، ستصبح العديد من عناصر واجهة المستخدم أكبر لاستيعاب تفاعل اللمس "
"بسهولة أكبر."

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr "نقرة مفردة لفتح الملفات"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr "سرعة التحريك"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr "نقرة بالزر الأيسر للفأرة في شريط التمرير يحركه صفحة واحدة"

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr "بدل تلقائيًا إلى الوضع المحَسن للمس"

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr "مكن النقر بالرز الأوسط ليلصق التحديد"

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr "أظهر عرض على الشاشة للإشارة إلى تغييرات الحالة مثل السطوع أو الصوت"

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr "إظهار عرض على الشاشة للإشارة لتغير المخطط"

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr "إظهار نافذة منبثقة على تغييرات التخطيط"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "زايد السعيدي"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zayed.alsaidi@gmail.com"

#~ msgid "General Behavior"
#~ msgstr "السلوك العام"

#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr "وحدة إعدادات النظام لضبط السلوك العام لمساحة العمل. "

#~ msgid "Furkan Tokac"
#~ msgstr "Furkan Tokac"

#~ msgid "Maintainer"
#~ msgstr "المصين"

#, fuzzy
#~| msgid "Show Informational Tips:"
#~ msgid "Show Informational Tips"
#~ msgstr "أظهر تلميحات المعلومات:"
