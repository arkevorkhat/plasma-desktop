# Translation of kimpanel into esperanto.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kimpanel\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2009-11-15 12:06+0100\n"
"Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: pology\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr ""

#: package/contents/ui/ActionMenu.qml:93
#, kde-format
msgid "(Empty)"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:29
#, kde-format
msgid "Input method list:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:30
#, kde-format
msgid "Vertical"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:36
#, kde-format
msgid "Horizontal"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:43
#, kde-format
msgid "Font:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:49
#, kde-format
msgid "Use custom:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:55
#, kde-format
msgctxt "The selected font family and font size"
msgid " "
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:67
#, kde-format
msgid "Select Font…"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:73
#, kde-format
msgctxt "The arrangement of icons in the Panel"
msgid "Panel icon size:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:74
#, kde-format
msgid "Small"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:81
#, kde-format
msgid "Scale with Panel height"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:82
#, kde-format
msgid "Scale with Panel width"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:90
#, kde-format
msgctxt "@title:window"
msgid "Select Font"
msgstr ""

#: package/contents/ui/ContextMenu.qml:83
#, kde-format
msgid "Show"
msgstr ""

#: package/contents/ui/ContextMenu.qml:98
#, kde-format
msgid "Hide %1"
msgstr ""

#: package/contents/ui/ContextMenu.qml:107
#, kde-format
msgid "Configure Input Method"
msgstr ""

#: package/contents/ui/ContextMenu.qml:115
#, kde-format
msgid "Reload Config"
msgstr ""

#: package/contents/ui/ContextMenu.qml:123
#, kde-format
msgid "Exit Input Method"
msgstr ""

#: package/contents/ui/main.qml:237
#, kde-format
msgid "Input Method Panel"
msgstr ""

#~ msgid "kimpanel"
#~ msgstr "kimpanel"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Axel Rousseau"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "axel@esperanto-jeunes.org"
