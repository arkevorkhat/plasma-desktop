# translation of kcminput.po to British English
# Copyright (C) 2002,2003, 2004, 2005, 2008 Free Software Foundation, Inc.
#
# Malcolm Hunter <malcolm.hunter@gmx.co.uk>, 2002,2003, 2005, 2008.
# Andrew Coles <andrew_coles@yahoo.co.uk>, 2004, 2005, 2009, 2010.
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2015, 2016, 2017, 2018, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:51+0000\n"
"PO-Revision-Date: 2022-08-29 12:12+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrew Coles, Steve Allewell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrew_coles@yahoo.co.uk, steve.allewell@gmail.com"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr "Querying input devices failed. Please reopen this settings module."

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr "Critical error on reading fundamental device infos of %1."

#: kcm/libinput/libinput_config.cpp:36
#, kde-format
msgid "Pointer device KCM"
msgstr "Pointer device KCM"

#: kcm/libinput/libinput_config.cpp:38
#, kde-format
msgid "System Settings module for managing mice and trackballs."
msgstr "System Settings module for managing mice and trackballs."

#: kcm/libinput/libinput_config.cpp:40
#, kde-format
msgid "Copyright 2018 Roman Gilg"
msgstr "Copyright 2018 Roman Gilg"

#: kcm/libinput/libinput_config.cpp:43 kcm/xlib/xlib_config.cpp:91
#, kde-format
msgid "Roman Gilg"
msgstr "Roman Gilg"

#: kcm/libinput/libinput_config.cpp:43
#, kde-format
msgid "Developer"
msgstr "Developer"

#: kcm/libinput/libinput_config.cpp:109
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."

#: kcm/libinput/libinput_config.cpp:114
#, kde-format
msgid "No pointer device found. Connect now."
msgstr "No pointer device found. Connect now."

#: kcm/libinput/libinput_config.cpp:125
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."

#: kcm/libinput/libinput_config.cpp:145
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""
"Error while loading default values. Failed to set some options to their "
"default values."

#: kcm/libinput/libinput_config.cpp:167
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."

#: kcm/libinput/libinput_config.cpp:191
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr "Pointer device disconnected. Closed its setting dialogue."

#: kcm/libinput/libinput_config.cpp:193
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr "Pointer device disconnected. No other devices found."

#: kcm/libinput/main.qml:79
#, kde-format
msgid "Device:"
msgstr "Device:"

#: kcm/libinput/main.qml:103 kcm/libinput/main_deviceless.qml:54
#, kde-format
msgid "General:"
msgstr "General:"

#: kcm/libinput/main.qml:105
#, kde-format
msgid "Device enabled"
msgstr "Device enabled"

#: kcm/libinput/main.qml:124
#, kde-format
msgid "Accept input through this device."
msgstr "Accept input through this device."

#: kcm/libinput/main.qml:130 kcm/libinput/main_deviceless.qml:56
#, kde-format
msgid "Left handed mode"
msgstr "Left handed mode"

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:75
#, kde-format
msgid "Swap left and right buttons."
msgstr "Swap left and right buttons."

#: kcm/libinput/main.qml:155 kcm/libinput/main_deviceless.qml:81
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr "Press left and right buttons for middle-click"

#: kcm/libinput/main.qml:174 kcm/libinput/main_deviceless.qml:100
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""
"Clicking left and right button simultaneously sends middle button click."

#: kcm/libinput/main.qml:184 kcm/libinput/main_deviceless.qml:110
#, kde-format
msgid "Pointer speed:"
msgstr "Pointer speed:"

#: kcm/libinput/main.qml:216 kcm/libinput/main_deviceless.qml:142
#, kde-format
msgid "Acceleration profile:"
msgstr "Acceleration profile:"

#: kcm/libinput/main.qml:247 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Flat"
msgstr "Flat"

#: kcm/libinput/main.qml:250 kcm/libinput/main_deviceless.qml:176
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr "Cursor moves the same distance as the mouse movement."

#: kcm/libinput/main.qml:257 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Adaptive"
msgstr "Adaptive"

#: kcm/libinput/main.qml:260 kcm/libinput/main_deviceless.qml:186
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr "Cursor travel distance depends on the mouse movement speed."

#: kcm/libinput/main.qml:272 kcm/libinput/main_deviceless.qml:198
#, kde-format
msgid "Scrolling:"
msgstr "Scrolling:"

#: kcm/libinput/main.qml:274 kcm/libinput/main_deviceless.qml:200
#, kde-format
msgid "Invert scroll direction"
msgstr "Invert scroll direction"

#: kcm/libinput/main.qml:289 kcm/libinput/main_deviceless.qml:215
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "Touchscreen like scrolling."

#: kcm/libinput/main.qml:295
#, kde-format
msgid "Scrolling speed:"
msgstr "Scrolling speed:"

#: kcm/libinput/main.qml:343
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "Slower"

#: kcm/libinput/main.qml:349
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "Faster"

#: kcm/libinput/main.qml:360
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr "Re-bind Additional Mouse Buttons…"

#: kcm/libinput/main.qml:396
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr "Extra Button %1:"

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr "Press the mouse button for which you want to add a key binding"

#: kcm/libinput/main.qml:427
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr "Enter the new key combination for %1"

#: kcm/libinput/main.qml:431
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Cancel"

#: kcm/libinput/main.qml:448
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "Press a mouse button "

#: kcm/libinput/main.qml:449
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr "Add Binding…"

#: kcm/libinput/main.qml:478
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr "Go back"

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "&General"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr "Button Order"

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr "Righ&t handed"

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr "Le&ft handed"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr "Re&verse scroll direction"

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "Advanced"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "Pointer acceleration:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr "Pointer threshold:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "Double click interval:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "Drag start time:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "Drag start distance:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr "Mouse wheel scrolls by:"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device).</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr " x"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognised as two separate clicks."

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr " msec"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, kde-format
msgid "Keyboard Navigation"
msgstr "Keyboard Navigation"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr "&Move pointer with keyboard (using the num pad)"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "&Acceleration delay:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr "R&epeat interval:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "Acceleration &time:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr "Ma&ximum speed:"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr " pixel/sec"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "Acceleration &profile:"

#: kcm/xlib/xlib_config.cpp:79
#, kde-format
msgid "Mouse"
msgstr "Mouse"

#: kcm/xlib/xlib_config.cpp:83
#, kde-format
msgid "(c) 1997 - 2018 Mouse developers"
msgstr "(c) 1997 - 2018 Mouse developers"

#: kcm/xlib/xlib_config.cpp:84
#, kde-format
msgid "Patrick Dowler"
msgstr "Patrick Dowler"

#: kcm/xlib/xlib_config.cpp:85
#, kde-format
msgid "Dirk A. Mueller"
msgstr "Dirk A. Mueller"

#: kcm/xlib/xlib_config.cpp:86
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kcm/xlib/xlib_config.cpp:87
#, kde-format
msgid "Bernd Gehrmann"
msgstr "Bernd Gehrmann"

#: kcm/xlib/xlib_config.cpp:88
#, kde-format
msgid "Rik Hemsley"
msgstr "Rik Hemsley"

#: kcm/xlib/xlib_config.cpp:89
#, kde-format
msgid "Brad Hughes"
msgstr "Brad Hughes"

#: kcm/xlib/xlib_config.cpp:90
#, kde-format
msgid "Brad Hards"
msgstr "Brad Hards"

#: kcm/xlib/xlib_config.cpp:283 kcm/xlib/xlib_config.cpp:288
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] " pixel"
msgstr[1] " pixels"

#: kcm/xlib/xlib_config.cpp:293
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] " line"
msgstr[1] " lines"

#~ msgid "Ralf Nolden"
#~ msgstr "Ralf Nolden"

#, fuzzy
#~| msgid "Acceleration &time:"
#~ msgid "Acceleration:"
#~ msgstr "Acceleration &time:"

#~ msgid "Acceleration Profile:"
#~ msgstr "Acceleration Profile:"

#~ msgid "Icons"
#~ msgstr "Icons"

#~ msgid ""
#~ "The default behavior in KDE is to select and activate icons with a single "
#~ "click of the left button on your pointing device. This behavior is "
#~ "consistent with what you would expect when you click links in most web "
#~ "browsers. If you would prefer to select with a single click, and activate "
#~ "with a double click, check this option."
#~ msgstr ""
#~ "The default behaviour in KDE is to select and activate icons with a "
#~ "single click of the left button on your pointing device. This behaviour "
#~ "is consistent with what you would expect when you click links in most web "
#~ "browsers. If you would prefer to select with a single click, and activate "
#~ "with a double click, tick this option."

#~ msgid ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"
#~ msgstr ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"

#~ msgid "Activates and opens a file or folder with a single click."
#~ msgstr "Activates and opens a file or folder with a single click."

#~ msgid "&Single-click to open files and folders"
#~ msgstr "&Single-click to open files and folders"

#~ msgid "Select the cursor theme you want to use:"
#~ msgstr "Select the cursor theme you want to use:"

#~ msgid "Name"
#~ msgstr "Name"

#~ msgid "Description"
#~ msgstr "Description"

#~ msgid "You have to restart KDE for these changes to take effect."
#~ msgstr "You have to restart KDE for these changes to take effect."

#~ msgid "Cursor Settings Changed"
#~ msgstr "Cursor Settings Changed"

#~ msgid "Small black"
#~ msgstr "Small black"

#~ msgid "Small black cursors"
#~ msgstr "Small black cursors"

#~ msgid "Large black"
#~ msgstr "Large black"

#~ msgid "Large black cursors"
#~ msgstr "Large black cursors"

#~ msgid "Small white"
#~ msgstr "Small white"

#~ msgid "Small white cursors"
#~ msgstr "Small white cursors"

#~ msgid "Large white"
#~ msgstr "Large white"

#~ msgid "Large white cursors"
#~ msgstr "Large white cursors"

#~ msgid "Cha&nge pointer shape over icons"
#~ msgstr "Cha&nge pointer shape over icons"

#~ msgid "A&utomatically select icons"
#~ msgstr "A&utomatically select icons"

#~ msgctxt "label. delay (on milliseconds) to automatically select icons"
#~ msgid "Delay"
#~ msgstr "Delay"

#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr " ms"

#~ msgid ""
#~ "If you check this option, pausing the mouse pointer over an icon on the "
#~ "screen will automatically select that icon. This may be useful when "
#~ "single clicks activate icons, and you want only to select the icon "
#~ "without activating it."
#~ msgstr ""
#~ "If you tick this option, pausing the mouse pointer over an icon on the "
#~ "screen will automatically select that icon. This may be useful when "
#~ "single clicks activate icons, and you want only to select the icon "
#~ "without activating it."

#~ msgid ""
#~ "If you have checked the option to automatically select icons, this slider "
#~ "allows you to select how long the mouse pointer must be paused over the "
#~ "icon before it is selected."
#~ msgstr ""
#~ "If you have ticked the option to automatically select icons, this slider "
#~ "allows you to select how long the mouse pointer must be paused over the "
#~ "icon before it is selected."

#~ msgid "Mouse type: %1"
#~ msgstr "Mouse type: %1"

#~ msgid ""
#~ "RF channel 1 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF channel 1 has been set. Please press Connect button on mouse to re-"
#~ "establish link"

#~ msgid ""
#~ "RF channel 2 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF channel 2 has been set. Please press Connect button on mouse to re-"
#~ "establish link"

#~ msgctxt "no cordless mouse"
#~ msgid "none"
#~ msgstr "none"

#~ msgid "Cordless Mouse"
#~ msgstr "Cordless Mouse"

#~ msgid "Cordless Wheel Mouse"
#~ msgstr "Cordless Wheel Mouse"

#~ msgid "Cordless MouseMan Wheel"
#~ msgstr "Cordless MouseMan Wheel"

#~ msgid "Cordless TrackMan Wheel"
#~ msgstr "Cordless TrackMan Wheel"

#~ msgid "TrackMan Live"
#~ msgstr "TrackMan Live"

#~ msgid "Cordless TrackMan FX"
#~ msgstr "Cordless TrackMan FX"

#~ msgid "Cordless MouseMan Optical"
#~ msgstr "Cordless MouseMan Optical"

#~ msgid "Cordless Optical Mouse"
#~ msgstr "Cordless Optical Mouse"

#~ msgid "Cordless MouseMan Optical (2ch)"
#~ msgstr "Cordless MouseMan Optical (2ch)"

#~ msgid "Cordless Optical Mouse (2ch)"
#~ msgstr "Cordless Optical Mouse (2ch)"

#~ msgid "Cordless Mouse (2ch)"
#~ msgstr "Cordless Mouse (2ch)"

#~ msgid "Cordless Optical TrackMan"
#~ msgstr "Cordless Optical TrackMan"

#~ msgid "MX700 Cordless Optical Mouse"
#~ msgstr "MX700 Cordless Optical Mouse"

#~ msgid "MX700 Cordless Optical Mouse (2ch)"
#~ msgstr "MX700 Cordless Optical Mouse (2ch)"

#~ msgid "Unknown mouse"
#~ msgstr "Unknown mouse"

#~ msgid "Cordless Name"
#~ msgstr "Cordless Name"

#~ msgid "Sensor Resolution"
#~ msgstr "Sensor Resolution"

#~ msgid "400 counts per inch"
#~ msgstr "400 counts per inch"

#~ msgid "800 counts per inch"
#~ msgstr "800 counts per inch"

#~ msgid "Battery Level"
#~ msgstr "Battery Level"

#~ msgid "RF Channel"
#~ msgstr "RF Channel"

#~ msgid "Channel 1"
#~ msgstr "Channel 1"

#~ msgid "Channel 2"
#~ msgstr "Channel 2"

#~ msgid ""
#~ "You have a Logitech Mouse connected, and libusb was found at compile "
#~ "time, but it was not possible to access this mouse. This is probably "
#~ "caused by a permissions problem - you should consult the manual on how to "
#~ "fix this."
#~ msgstr ""
#~ "You have a Logitech Mouse connected, and libusb was found at compile "
#~ "time, but it was not possible to access this mouse. This is probably "
#~ "caused by a permissions problem - you should consult the manual on how to "
#~ "fix this."

#~ msgid "Cursor Theme"
#~ msgstr "Cursor Theme"

#~ msgid "(c) 2003-2007 Fredrik Höglund"
#~ msgstr "(c) 2003-2007 Fredrik Höglund"

#~ msgid "Fredrik Höglund"
#~ msgstr "Fredrik Höglund"

#~ msgctxt "@item:inlistbox size"
#~ msgid "Resolution dependent"
#~ msgstr "Resolution dependent"

#~ msgid "Drag or Type Theme URL"
#~ msgstr "Drag or Type Theme URL"

#~ msgid "Unable to find the cursor theme archive %1."
#~ msgstr "Unable to find the cursor theme archive %1."

#~ msgid ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."
#~ msgstr ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."

#~ msgid "The file %1 does not appear to be a valid cursor theme archive."
#~ msgstr "The file %1 does not appear to be a valid cursor theme archive."

#~ msgid ""
#~ "<qt>You cannot delete the theme you are currently using.<br />You have to "
#~ "switch to another theme first.</qt>"
#~ msgstr ""
#~ "<qt>You cannot delete the theme you are currently using.<br />You have to "
#~ "switch to another theme first.</qt>"

#~ msgid ""
#~ "<qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This "
#~ "will delete all the files installed by this theme.</qt>"
#~ msgstr ""
#~ "<qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This "
#~ "will delete all the files installed by this theme.</qt>"

#~ msgid "Confirmation"
#~ msgstr "Confirmation"

#~ msgid ""
#~ "A theme named %1 already exists in your icon theme folder. Do you want "
#~ "replace it with this one?"
#~ msgstr ""
#~ "A theme named %1 already exists in your icon theme folder. Do you want "
#~ "replace it with this one?"

#~ msgid "Overwrite Theme?"
#~ msgstr "Overwrite Theme?"

#~ msgid ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"
#~ msgstr ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"

#~ msgid "Get new color schemes from the Internet"
#~ msgstr "Get new colour schemes from the Internet"

#~ msgid "Get New Theme..."
#~ msgstr "Get New Theme..."

#~ msgid "Install From File..."
#~ msgstr "Install From File..."

#~ msgid "Remove Theme"
#~ msgstr "Remove Theme"

#~ msgctxt "@label:listbox cursor size"
#~ msgid "Size:"
#~ msgstr "Size:"

#~ msgctxt ""
#~ "@info The argument is the list of available sizes (in pixel). Example: "
#~ "'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
#~ msgid "(Available sizes: %1)"
#~ msgstr "(Available sizes: %1)"

#~ msgid "KDE Classic"
#~ msgstr "KDE Classic"

#~ msgid "The default cursor theme in KDE 2 and 3"
#~ msgstr "The default cursor theme in KDE 2 and 3"

#~ msgid "No description available"
#~ msgstr "No description available"

#~ msgid "Short"
#~ msgstr "Short"

#~ msgid "Long"
#~ msgstr "Long"

#~ msgid "Show feedback when clicking an icon"
#~ msgstr "Show feedback when clicking an icon"

#~ msgid "Visual f&eedback on activation"
#~ msgstr "Visual f&eedback on activation"

#~ msgid "No theme"
#~ msgstr "No theme"

#~ msgid "The old classic X cursors"
#~ msgstr "The old classic X cursors"

#~ msgid "System theme"
#~ msgstr "System theme"

#~ msgid "Do not change cursor theme"
#~ msgstr "Do not change cursor theme"
