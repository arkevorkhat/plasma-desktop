# Irish translation of plasma_applet_trash
# Copyright (C) 2009 This_file_is_part_of_KDE
# This file is distributed under the same license as the plasma_applet_trash package.
# Kevin Scannell <kscanne@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kdereview/plasma_applet_trash.po\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2008-02-12 09:30-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#: contents/ui/main.qml:100
#, fuzzy, kde-format
#| msgid "&Open"
msgctxt "a verb"
msgid "Open"
msgstr "&Oscail"

#: contents/ui/main.qml:101
#, fuzzy, kde-format
#| msgctxt "The trash is empty. This is not an action, but a state"
#| msgid "Empty"
msgctxt "a verb"
msgid "Empty"
msgstr "Folamh"

#: contents/ui/main.qml:107
#, kde-format
msgid "Trash Settings…"
msgstr ""

#: contents/ui/main.qml:158
#, fuzzy, kde-format
#| msgid "Trash"
msgid ""
"Trash\n"
"Empty"
msgstr "Bruscar"

#: contents/ui/main.qml:158
#, fuzzy, kde-format
#| msgid "One item"
#| msgid_plural "%1 items"
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] "Mír amháin"
msgstr[1] "%1 mhír"
msgstr[2] "%1 mhír"
msgstr[3] "%1 mír"
msgstr[4] "%1 mír"

#: contents/ui/main.qml:167
#, kde-format
msgid "Trash"
msgstr "Bruscar"

#: contents/ui/main.qml:168
#, fuzzy, kde-format
#| msgctxt "The trash is empty. This is not an action, but a state"
#| msgid "Empty"
msgid "Empty"
msgstr "Folamh"

#: contents/ui/main.qml:168
#, fuzzy, kde-format
#| msgid "One item"
#| msgid_plural "%1 items"
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "Mír amháin"
msgstr[1] "%1 mhír"
msgstr[2] "%1 mhír"
msgstr[3] "%1 mír"
msgstr[4] "%1 mír"

#~ msgid "Empty Trash"
#~ msgstr "Folmhaigh an Bruscar"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "Do you really want to empty the trash? All items will be deleted."
#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr ""
#~ "An bhfuil tú cinnte gur mian leat an bruscar a fholmhú?  Scriosfar gach "
#~ "mír."

#~ msgid "Cancel"
#~ msgstr "Cealaigh"

#~ msgid "&Empty Trashcan"
#~ msgstr "&Folmhaigh an Bruscar"

#~ msgid "&Menu"
#~ msgstr "&Roghchlár"

#~ msgctxt "@title:window"
#~ msgid "Empty Trash"
#~ msgstr "Folmhaigh an Bruscar"

#~ msgid "Emptying Trashcan..."
#~ msgstr "Bruscar á Fholmhú..."
