# translation of kcminput.po to Macedonian
#
# Copyright (C) 2000,2002,2003, 2004, 2005, 2006, 2007, 2009, 2010 Free Software Foundation, Inc.
#
# Danko Ilik <danko@mindless.com>, 2000,2002,2003.
# Novica Nakov <novica@bagra.net.mk>, 2003.
# Bozidar Proevski <bobibobi@freemail.com.mk>, 2004, 2005, 2006, 2007, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:51+0000\n"
"PO-Revision-Date: 2010-01-29 00:39+0100\n"
"Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>\n"
"Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : "
"2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Божидар Проевски"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bobibobi@freemail.com.mk"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:36
#, kde-format
msgid "Pointer device KCM"
msgstr ""

#: kcm/libinput/libinput_config.cpp:38
#, kde-format
msgid "System Settings module for managing mice and trackballs."
msgstr ""

#: kcm/libinput/libinput_config.cpp:40
#, kde-format
msgid "Copyright 2018 Roman Gilg"
msgstr ""

#: kcm/libinput/libinput_config.cpp:43 kcm/xlib/xlib_config.cpp:91
#, kde-format
msgid "Roman Gilg"
msgstr ""

#: kcm/libinput/libinput_config.cpp:43
#, kde-format
msgid "Developer"
msgstr ""

#: kcm/libinput/libinput_config.cpp:109
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:114
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:125
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:145
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:167
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:191
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:193
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:79
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/main.qml:103 kcm/libinput/main_deviceless.qml:54
#, fuzzy, kde-format
#| msgid "&General"
msgid "General:"
msgstr "&Општо"

#: kcm/libinput/main.qml:105
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:124
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:130 kcm/libinput/main_deviceless.qml:56
#, fuzzy, kde-format
#| msgid "Le&ft handed"
msgid "Left handed mode"
msgstr "За лева ра&ка"

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:75
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:155 kcm/libinput/main_deviceless.qml:81
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:174 kcm/libinput/main_deviceless.qml:100
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:184 kcm/libinput/main_deviceless.qml:110
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Pointer speed:"
msgstr "Праг на покажувачот:"

#: kcm/libinput/main.qml:216 kcm/libinput/main_deviceless.qml:142
#, fuzzy, kde-format
#| msgid "Acceleration &profile:"
msgid "Acceleration profile:"
msgstr "&Профил на забрзување:"

#: kcm/libinput/main.qml:247 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Flat"
msgstr ""

#: kcm/libinput/main.qml:250 kcm/libinput/main_deviceless.qml:176
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:257 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Adaptive"
msgstr ""

#: kcm/libinput/main.qml:260 kcm/libinput/main_deviceless.qml:186
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:272 kcm/libinput/main_deviceless.qml:198
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:274 kcm/libinput/main_deviceless.qml:200
#, fuzzy, kde-format
#| msgid "Re&verse scroll direction"
msgid "Invert scroll direction"
msgstr "С&врти ја насоката на движење"

#: kcm/libinput/main.qml:289 kcm/libinput/main_deviceless.qml:215
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:295
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Scrolling speed:"
msgstr "Праг на покажувачот:"

#: kcm/libinput/main.qml:343
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:349
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:360
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:396
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:427
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:431
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:448
#, fuzzy, kde-format
#| msgid "Press Connect Button"
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "Притиснете на копчето Поврзи"

#: kcm/libinput/main.qml:449
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:478
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""
"<h1>Глушец</h1> Овој модул ви овозможува да изберете различни опции за "
"начинот на кој работи вашиот уред за покажување. Уредот за покажување може "
"да биде глушец, топка, или некој друг уред кој ја врши истата функција."

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "&Општо"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""
"Ако сте левораки, можеби претпочитате да ги замените функциите на левите и "
"десните копчиња на вашиот уред за покажување со избирање на опцијата "
"„Леворак“. Ако вашиот уред за покажување има повеќе од две копчиња, тогаш се "
"менуваат само тие што имаат функција на леви и десни. На пример ако имате "
"глушец со три копчиња, тогаш промената нема ефект врз средното копче."

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr "Редослед на копчиња"

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr "За де&сна рака"

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr "За лева ра&ка"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr ""
"Ја менува насоката на движење за тркалцето на глушецот или за четвртото и "
"петтото копче на глушецот."

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr "С&врти ја насоката на движење"

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "Напредно"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "Забрзување на покажувачот:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr "Праг на покажувачот:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "Интервал за двоен клик:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "Најмало време за влечење:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "Најмало растојание за влечење:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr "Тркалцето движи за:"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""
"<p>Оваа опција ви овозможува да го смените односот помеѓу растојанието кое "
"го поминува покажувачот на екранот и релативното движење на самиот физички "
"уред (кој може да биде глушец, топка, или некој друг покажувачки уред.)</"
"p><p> Висока вредност за забрзувањето ќе предизвика големи движења на "
"покажувачот на екранот дури и кога правите мали движења со физичкиот уред. "
"Избирањето на високи вредности ќе предизвика покажувачот да лета по екранот "
"и со тоа да биде тешко да се контролира истиот.</p>"

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr " x"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""
"<p>Прагот е најмалото растојание кое мора да го мине покажувачот на екранот, "
"пред да настапи забрзувањето. Ако движењето е помало од прагот, покажувачот "
"ќе се движи како забрзувањето да е 1X.</p><p> Така, кога правите мали "
"движења со физичкиот уред, нема никакво забрзување, со што имате зголемена "
"контрола над покажувачот. Со поголеми движења на физичкиот уред, можете брзо "
"да го движите покажувачот до различни делови на екранот.</p>"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""
"Интервалот за двојно кликање е максималното време (во милисекунди) помеѓу "
"два клика на глушецот кое што ги прави во двоен клик. Ако вториот клик се "
"случи подоцна од овој временски интервал, двата се сметаат како посебни "
"единечни кликови."

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr " msec"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""
"Ако кликнете со глушецот (на пр. во повеќелиниски уредувач) и почнете да го "
"движите во рамките на времето за влечење, ќе се иницира операција на влечење."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""
"Ако кликнете со глушецот и го придвижите на најмалото растојание за влечење, "
"ќе се иницира операција на влечење."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""
"Ако го користите тркалцето на глушецот, оваа вредност утврдува колку линии "
"ќе се поместуваат со секое движење на тркалцето. Забележете дека ако овој "
"број го надмине бројот на видливи линии, истиот ќе биде игнориран и "
"движењето на тркалцето ќе се смета како движење со page up/down."

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, fuzzy, kde-format
#| msgid "Mouse Navigation"
msgid "Keyboard Navigation"
msgstr "Навигација со глушецот"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr "Д&вижи го глушецот со тастатура (користејќи ја нумеричката тастатура)"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "Доцнење на з&абрзување:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr "Интервал на повто&рување:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "Вр&еме на забрзување:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr "&Максимална брзина:"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr " пиксели/сек"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "&Профил на забрзување:"

#: kcm/xlib/xlib_config.cpp:79
#, kde-format
msgid "Mouse"
msgstr "Глушец"

#: kcm/xlib/xlib_config.cpp:83
#, fuzzy, kde-format
#| msgid "(c) 1997 - 2005 Mouse developers"
msgid "(c) 1997 - 2018 Mouse developers"
msgstr "(c) 1997 - 2005 Развивачите на „Глушец“"

#: kcm/xlib/xlib_config.cpp:84
#, kde-format
msgid "Patrick Dowler"
msgstr "Patrick Dowler"

#: kcm/xlib/xlib_config.cpp:85
#, kde-format
msgid "Dirk A. Mueller"
msgstr "Dirk A. Mueller"

#: kcm/xlib/xlib_config.cpp:86
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kcm/xlib/xlib_config.cpp:87
#, kde-format
msgid "Bernd Gehrmann"
msgstr "Bernd Gehrmann"

#: kcm/xlib/xlib_config.cpp:88
#, kde-format
msgid "Rik Hemsley"
msgstr "Rik Hemsley"

#: kcm/xlib/xlib_config.cpp:89
#, kde-format
msgid "Brad Hughes"
msgstr "Brad Hughes"

#: kcm/xlib/xlib_config.cpp:90
#, kde-format
msgid "Brad Hards"
msgstr "Brad Hards"

#: kcm/xlib/xlib_config.cpp:283 kcm/xlib/xlib_config.cpp:288
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] " пиксел"
msgstr[1] " пиксели"
msgstr[2] " пиксели"

#: kcm/xlib/xlib_config.cpp:293
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] " линија"
msgstr[1] " линии"
msgstr[2] " линии"

#~ msgid "Ralf Nolden"
#~ msgstr "Ralf Nolden"

#, fuzzy
#~| msgid "Acceleration &time:"
#~ msgid "Acceleration:"
#~ msgstr "Вр&еме на забрзување:"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration Profile:"
#~ msgstr "&Профил на забрзување:"

#~ msgid "Icons"
#~ msgstr "Икони"

#~ msgid ""
#~ "The default behavior in KDE is to select and activate icons with a single "
#~ "click of the left button on your pointing device. This behavior is "
#~ "consistent with what you would expect when you click links in most web "
#~ "browsers. If you would prefer to select with a single click, and activate "
#~ "with a double click, check this option."
#~ msgstr ""
#~ "Стандардното однесување во KDE е да избирате и активирате икони со еден "
#~ "клик со левото копче на вашиот покажувачки уред. Ова однесување е во "
#~ "склад со она што се случува кога кликате на линкови во повеќето веб "
#~ "прелистувачи. Ако претпочитате да избирате со еден клик, а да активирате "
#~ "со два клика, изберете ја оваа опција."

#~ msgid ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"
#~ msgstr ""
#~ "Двоен к&лик за отворање на датотеки и папки (икони се избираат со еден "
#~ "клик)"

#~ msgid "Activates and opens a file or folder with a single click."
#~ msgstr "Активира и отвара датотека или папка со еден клик."

#~ msgid "&Single-click to open files and folders"
#~ msgstr "&Еден клик за отворање на датотеки и папки"

#~ msgid "Select the cursor theme you want to use:"
#~ msgstr "Изберете ја темата ка курсорот која сакате да ја користите:"

#~ msgid "Name"
#~ msgstr "Име"

#~ msgid "Description"
#~ msgstr "Опис"

#~ msgid "You have to restart KDE for these changes to take effect."
#~ msgstr ""
#~ "Мора да го рестартирате KDE за промената на бојата на курсорот да има "
#~ "ефект."

#~ msgid "Cursor Settings Changed"
#~ msgstr "Поставувањата за курсорот се изменети"

#~ msgid "Small black"
#~ msgstr "Мали црни"

#~ msgid "Small black cursors"
#~ msgstr "Мали црни курсори"

#~ msgid "Large black"
#~ msgstr "Големи црни"

#~ msgid "Large black cursors"
#~ msgstr "Големи црни курсори"

#~ msgid "Small white"
#~ msgstr "Мали бели"

#~ msgid "Small white cursors"
#~ msgstr "Мали бели курсори"

#~ msgid "Large white"
#~ msgstr "Големи бели"

#~ msgid "Large white cursors"
#~ msgstr "Големи бели курсори"

#~ msgid "Cha&nge pointer shape over icons"
#~ msgstr "Измени ја формата на &покажувачот над иконите"

#~ msgid "A&utomatically select icons"
#~ msgstr "Авт&оматски избери икони"

#, fuzzy
#~| msgid "Dela&y:"
#~ msgctxt "label. delay (on milliseconds) to automatically select icons"
#~ msgid "Delay"
#~ msgstr "Доцн&ење:"

#, fuzzy
#~| msgid " msec"
#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr " msec"

#~ msgid ""
#~ "If you check this option, pausing the mouse pointer over an icon on the "
#~ "screen will automatically select that icon. This may be useful when "
#~ "single clicks activate icons, and you want only to select the icon "
#~ "without activating it."
#~ msgstr ""
#~ "Ако ја изберете оваа опција, со самото застанување на покажувачот на "
#~ "глушецот врз икона истата се избира. Ова е корисно кога иконите се "
#~ "активираат со еден клик, а вие само сакате да изберете икона без да ја "
#~ "активирате."

#~ msgid ""
#~ "If you have checked the option to automatically select icons, this slider "
#~ "allows you to select how long the mouse pointer must be paused over the "
#~ "icon before it is selected."
#~ msgstr ""
#~ "Ако ја избравте опцијата за автоматско избирање на икони, овој лизгач ви "
#~ "овозможува да поставите колку долго покажувачот на глушецот треба да биде "
#~ "над иконата пред таа да биде избрана."

#~ msgid "Mouse type: %1"
#~ msgstr "Тип на глушец: %1"

#~ msgid ""
#~ "RF channel 1 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF-каналот 1 е поставен. Притиснете го копчето Поврзи на глушецот за "
#~ "повторно да воспоставите врска"

#~ msgid ""
#~ "RF channel 2 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF-каналот 2 е поставен. Притиснете го копчето Поврзи на глушецот за "
#~ "повторно да воспоставите врска"

#~ msgctxt "no cordless mouse"
#~ msgid "none"
#~ msgstr "нема"

#~ msgid "Cordless Mouse"
#~ msgstr "Бежичен глушец"

#~ msgid "Cordless Wheel Mouse"
#~ msgstr "Бежичен глушец со тркалце"

#~ msgid "Cordless MouseMan Wheel"
#~ msgstr "Бежичен MouseMan со тркалце"

#~ msgid "Cordless TrackMan Wheel"
#~ msgstr "Бежичен TrackMan со тркалце"

#~ msgid "TrackMan Live"
#~ msgstr "TrackMan Live"

#~ msgid "Cordless TrackMan FX"
#~ msgstr "Бежичен TrackMan FX"

#~ msgid "Cordless MouseMan Optical"
#~ msgstr "Бежичен MouseMan оптички"

#~ msgid "Cordless Optical Mouse"
#~ msgstr "Бежичен оптички глушец"

#~ msgid "Cordless MouseMan Optical (2ch)"
#~ msgstr "Бежичен MouseMan глушец (2ch)"

#~ msgid "Cordless Optical Mouse (2ch)"
#~ msgstr "Бежичен оптички глушец (2ch)"

#~ msgid "Cordless Mouse (2ch)"
#~ msgstr "Бежичен глушец (2ch)"

#~ msgid "Cordless Optical TrackMan"
#~ msgstr "Бежичен оптички TrackMan"

#~ msgid "MX700 Cordless Optical Mouse"
#~ msgstr "Бежичен оптички глушец MX700"

#~ msgid "MX700 Cordless Optical Mouse (2ch)"
#~ msgstr "Бежичен оптички глушец MX700 (2ch)"

#~ msgid "Unknown mouse"
#~ msgstr "Непознат глушец"

#~ msgid "Cordless Name"
#~ msgstr "Име на бежичен"

#~ msgid "Sensor Resolution"
#~ msgstr "Резолуција на сензорот"

#~ msgid "400 counts per inch"
#~ msgstr "400 броења по инч"

#~ msgid "800 counts per inch"
#~ msgstr "800 броења по инч"

#~ msgid "Battery Level"
#~ msgstr "Ниво на батерија"

#~ msgid "RF Channel"
#~ msgstr "RF-канал"

#~ msgid "Channel 1"
#~ msgstr "Канал 1"

#~ msgid "Channel 2"
#~ msgstr "Канал 2"

#~ msgid ""
#~ "You have a Logitech Mouse connected, and libusb was found at compile "
#~ "time, but it was not possible to access this mouse. This is probably "
#~ "caused by a permissions problem - you should consult the manual on how to "
#~ "fix this."
#~ msgstr ""
#~ "Вие имате поврзан глушец Logitech, и libusb беше пронајдена за време на "
#~ "преведувањето, не не беше можно да се пристапи на глушецот. Можно е ова "
#~ "да е причинето од проблем со дозволите - консултирајте се со прирачникот "
#~ "како да го поправите ова."

#, fuzzy
#~| msgid "&Cursor Theme"
#~ msgid "Cursor Theme"
#~ msgstr "&Тема на курсор"

#~ msgid "Drag or Type Theme URL"
#~ msgstr "Довлечи или впиши URL на тема"

#~ msgid "Unable to find the cursor theme archive %1."
#~ msgstr "Не може да ја пронајдам архивата со теми на курсори %1."

#~ msgid ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."
#~ msgstr ""
#~ "Не може да се симне архивата со теми на курсори; ве молиме проверете дали "
#~ "е точна адресата %1."

#~ msgid "The file %1 does not appear to be a valid cursor theme archive."
#~ msgstr "Изгледа дека датотеката %1 не е валидна архива со теми на курсори."

#~ msgid ""
#~ "<qt>You cannot delete the theme you are currently using.<br />You have to "
#~ "switch to another theme first.</qt>"
#~ msgstr ""
#~ "<qt>Не може да ја избришете темата што тековно ја користите.<br />Прво "
#~ "треба да се префрлите на друга тема.</qt>"

#~ msgid ""
#~ "<qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This "
#~ "will delete all the files installed by this theme.</qt>"
#~ msgstr ""
#~ "<qt>Дали навистина сакате да ја отстраните темата на курсори <i>%1</i>?"
#~ "<br />Ова ќе ги избрише сите датотеки инсталирани од оваа тема.</qt>"

#~ msgid "Confirmation"
#~ msgstr "Потврда"

#~ msgid ""
#~ "A theme named %1 already exists in your icon theme folder. Do you want "
#~ "replace it with this one?"
#~ msgstr ""
#~ "Во вашата папка со теми на икони веќе постои тема наречена %1. Дали "
#~ "сакате да ја замените со оваа?"

#~ msgid "Overwrite Theme?"
#~ msgstr "Да запишам врз темата?"

#~ msgid ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"
#~ msgstr ""
#~ "Изберете ја темата за курсор што сакате да ја користите (застанете над "
#~ "прегледот за да ја тестирате):"

#, fuzzy
#~| msgid "Install New Theme..."
#~ msgid "Get New Theme..."
#~ msgstr "Инсталирај нова тема..."

#, fuzzy
#~| msgid "Install New Theme..."
#~ msgid "Install From File..."
#~ msgstr "Инсталирај нова тема..."

#~ msgid "Remove Theme"
#~ msgstr "Отстранување тема"

#~ msgid "KDE Classic"
#~ msgstr "KDE Класична"

#~ msgid "The default cursor theme in KDE 2 and 3"
#~ msgstr "Стандардната тема за покажувачи во KDE 2 и 3"

#~ msgid "No description available"
#~ msgstr "Нема достапен опис"

#~ msgid "Short"
#~ msgstr "Кусо"

#~ msgid "Long"
#~ msgstr "Долго"
