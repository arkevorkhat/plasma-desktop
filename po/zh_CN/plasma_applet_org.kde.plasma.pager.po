msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2023-02-15 11:05\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasma-desktop/plasma_applet_org.kde."
"plasma.pager.pot\n"
"X-Crowdin-File-ID: 4116\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "常规"

#: package/contents/ui/configGeneral.qml:40
#, kde-format
msgid "General:"
msgstr "常规："

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgid "Show application icons on window outlines"
msgstr "显示应用图标到窗口轮廓中"

#: package/contents/ui/configGeneral.qml:47
#, kde-format
msgid "Show only current screen"
msgstr "只显示当前屏幕"

#: package/contents/ui/configGeneral.qml:52
#, kde-format
msgid "Navigation wraps around"
msgstr "循环切换"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Layout:"
msgstr "布局："

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgctxt "The pager layout"
msgid "Default"
msgstr "默认"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Horizontal"
msgstr "垂直"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Vertical"
msgstr "水平"

#: package/contents/ui/configGeneral.qml:80
#, kde-format
msgid "Text display:"
msgstr "文本显示："

#: package/contents/ui/configGeneral.qml:83
#, kde-format
msgid "No text"
msgstr "无文本"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Activity number"
msgstr "活动编号"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Desktop number"
msgstr "桌面编号"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Activity name"
msgstr "活动名称"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Desktop name"
msgstr "桌面名称"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current Activity:"
msgstr "选择当前活动："

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current virtual desktop:"
msgstr "选择当前虚拟桌面："

#: package/contents/ui/configGeneral.qml:116
#, kde-format
msgid "Does nothing"
msgstr "无操作"

#: package/contents/ui/configGeneral.qml:124
#, kde-format
msgid "Shows the desktop"
msgstr "显示桌面"

#: package/contents/ui/main.qml:104
#, kde-format
msgid "…and %1 other window"
msgid_plural "…and %1 other windows"
msgstr[0] "…及其他 %1 个窗口"

#: package/contents/ui/main.qml:362
#, kde-format
msgid "%1 Window:"
msgid_plural "%1 Windows:"
msgstr[0] "%1 个窗口："

#: package/contents/ui/main.qml:374
#, kde-format
msgid "%1 Minimized Window:"
msgid_plural "%1 Minimized Windows:"
msgstr[0] "%1 个最小化窗口："

#: package/contents/ui/main.qml:449
#, kde-format
msgid "Desktop %1"
msgstr "桌面 %1"

#: package/contents/ui/main.qml:450
#, kde-format
msgctxt "@info:tooltip %1 is the name of a virtual desktop or an activity"
msgid "Switch to %1"
msgstr "切换到 %1"

#: package/contents/ui/main.qml:595
#, kde-format
msgid "Show Activity Manager…"
msgstr "显示活动管理器…"

#: package/contents/ui/main.qml:598
#, kde-format
msgid "Add Virtual Desktop"
msgstr "添加虚拟桌面"

#: package/contents/ui/main.qml:599
#, kde-format
msgid "Remove Virtual Desktop"
msgstr "移除虚拟桌面"

#: package/contents/ui/main.qml:602
#, kde-format
msgid "Configure Virtual Desktops…"
msgstr "配置虚拟桌面…"
