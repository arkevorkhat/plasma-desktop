# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-12 00:48+0000\n"
"PO-Revision-Date: 2022-02-14 07:12+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.3\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#. i18n: ectx: label, entry (colorScheme), group (General)
#: landingpage_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Color scheme name"
msgstr "Ime barvne sheme"

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:15
#, kde-format
msgid "Single click to open files"
msgstr "Enojni klik odpira datoteke"

#. i18n: ectx: label, entry (lookAndFeelPackage), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:19
#, kde-format
msgid "Global Look and Feel package"
msgstr "Paket globalnega videza in občutka"

#. i18n: ectx: label, entry (defaultLightLookAndFeel), group (KDE)
#. i18n: ectx: label, entry (defaultDarkLookAndFeel), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:23
#: landingpage_kdeglobalssettings.kcfg:27
#, kde-format
msgid "Global Look and Feel package, alternate"
msgstr "Paket globalnega videza in občutka, alternativni"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:31
#, kde-format
msgid "Animation speed"
msgstr "Hitrost animacije"

#: package/contents/ui/main.qml:27
#, kde-format
msgid "Theme:"
msgstr "Tema:"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Animation speed:"
msgstr "Hitrost animacije:"

#: package/contents/ui/main.qml:91
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Počasna"

#: package/contents/ui/main.qml:97
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Trenutna"

#: package/contents/ui/main.qml:111
#, kde-format
msgid "Change Wallpaper…"
msgstr "Spremeni ozadje…"

#: package/contents/ui/main.qml:118
#, kde-format
msgid "More Appearance Settings…"
msgstr "Več nastavitev izgleda…"

#: package/contents/ui/main.qml:133
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Klikanje na datoteke ali mape:"

#: package/contents/ui/main.qml:134
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Jih odpre"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Izberi s klikom na označevalnik predmeta"

#: package/contents/ui/main.qml:158
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Jih izbere"

#: package/contents/ui/main.qml:172
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Odpri z dvojnim klikom"

#: package/contents/ui/main.qml:184
#, kde-format
msgid "More Behavior Settings…"
msgstr "Več nastavitev obnašanja…"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "Most Used Pages:"
msgstr "Najpogostejše uporabljane strani:"

#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgid ""
#~ "You can help KDE improve Plasma by contributing information on how you "
#~ "use it, so we can focus on things that matter to you.<br/><br/"
#~ ">Contributing this information is optional and entirely anonymous. We "
#~ "never collect your personal data, files you use, websites you visit, or "
#~ "information that could identify you."
#~ msgstr ""
#~ "KDEju lahko pomagate izboljšati Plasmo tako, da prispevate informacije o "
#~ "tem, kako jo uporabljate, zato da se lahko osredotočimo na stvari, ki so "
#~ "za vas pomembne.<br/><br/>Prispevanje teh informacij ni obvezno in je "
#~ "popolnoma anonimno. Nikoli ne zbiramo vaših osebnih podatkov, datotek, ki "
#~ "jih uporabljate, spletnih mest, ki jih obiskujete, ali informacij, ki bi "
#~ "vas lahko identificirale."

#~ msgid "No data will be sent."
#~ msgstr "Nobenih podatkov ne bo poslano."

#~ msgid "The following information will be sent:"
#~ msgstr "Poslane bodo naslednje informacije:"

#~ msgid "Send User Feedback:"
#~ msgstr "Pošlji povratne informacije uporabnika:"

#~ msgctxt "Adjective; as in, 'light theme'"
#~ msgid "Light"
#~ msgstr "Svetla"

#~ msgctxt "Adjective; as in, 'dark theme'"
#~ msgid "Dark"
#~ msgstr "Temna"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Matjaž Jeran"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "matjaz.jeran@amis.net"

#~ msgid "Quick Settings"
#~ msgstr "Hitre nastavitve"

#~ msgid "Landing page with some basic settings."
#~ msgstr "Pristajalna stran z nekaterimi osnovnimi nastavitvami."

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Most used module number %1"
#~ msgstr "Najpogosteje uporabljan modul številka %1"

#~ msgid "Enable file indexer"
#~ msgstr "Omogoči indeksirnik datotek"

#~ msgid "File Indexing:"
#~ msgstr "Indeksiranje datoteke:"

#~ msgid "Enabled"
#~ msgstr "Omogočeno"
