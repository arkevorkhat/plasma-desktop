msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-05 00:46+0000\n"
"PO-Revision-Date: 2021-01-26 10:29+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: pt <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: NumLock Tab org ms AltGr Lock Enter Backspace Caps\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-IgnoreConsistency: Enter\n"
"X-POFile-SpellExtra: XKB Rysin Andriy\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: bindings.cpp:24
#, kde-format
msgid "Keyboard Layout Switcher"
msgstr "Selector de Disposições do Teclado"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "Mudar para a Disposição do Teclado Seguinte"

#: bindings.cpp:49
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr "Mudar a disposição do teclado para %1"

#: flags.cpp:122
#, kde-format
msgctxt "layout - variant"
msgid "%1 - %2"
msgstr "%1 - %2"

#. i18n: ectx: property (windowTitle), widget (QDialog, AddLayoutDialog)
#: kcm_add_layout_dialog.ui:14
#, kde-format
msgid "Add Layout"
msgstr "Adicionar uma Disposição"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, layoutSearchField)
#: kcm_add_layout_dialog.ui:20
#, kde-format
msgid "Search…"
msgstr "Procurar…"

#. i18n: ectx: property (text), widget (QLabel, shortcutLabel)
#: kcm_add_layout_dialog.ui:45
#, kde-format
msgid "Shortcut:"
msgstr "Atalho:"

#. i18n: ectx: property (text), widget (QLabel, labelLabel)
#: kcm_add_layout_dialog.ui:55
#, kde-format
msgid "Label:"
msgstr "Nome:"

#. i18n: ectx: property (text), widget (QPushButton, prevbutton)
#. i18n: ectx: property (text), widget (QPushButton, previewButton)
#: kcm_add_layout_dialog.ui:76 kcm_keyboard.ui:315
#, kde-format
msgid "Preview"
msgstr "Antevisão"

#: kcm_keyboard.cpp:32
#, kde-format
msgid "KDE Keyboard Control Module"
msgstr "Módulo de Controlo do Teclado do KDE"

#: kcm_keyboard.cpp:36
#, kde-format
msgid "(c) 2010 Andriy Rysin"
msgstr "(c) 2010 Andriy Rysin"

#: kcm_keyboard.cpp:40
#, kde-format
msgid ""
"<h1>Keyboard</h1> This control module can be used to configure keyboard "
"parameters and layouts."
msgstr ""
"<h1>Teclado</h1> Este módulo de controlo pode ser usado para configurar os "
"parâmetros e disposições do teclado."

#. i18n: ectx: attribute (title), widget (QWidget, tabHardware)
#: kcm_keyboard.ui:18
#, kde-format
msgid "Hardware"
msgstr "'Hardware'"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm_keyboard.ui:33
#, kde-format
msgid "Keyboard &model:"
msgstr "&Modelo do teclado:"

#. i18n: ectx: property (whatsThis), widget (QComboBox, keyboardModelComboBox)
#: kcm_keyboard.ui:53
#, kde-format
msgid ""
"Here you can choose a keyboard model. This setting is independent of your "
"keyboard layout and refers to the \"hardware\" model, i.e. the way your "
"keyboard is manufactured. Modern keyboards that come with your computer "
"usually have two extra keys and are referred to as \"104-key\" models, which "
"is probably what you want if you do not know what kind of keyboard you "
"have.\n"
msgstr ""
"Aqui pode escolher um modelo de teclado. Esta configuração é independente da "
"sua configuração de teclado e refere-se ao modelo de \"hardware\", i.e. a "
"maneira como o teclado é fabricado. Os teclados modernos que vêm com o seu "
"computador normalmente têm duas teclas extra e são referidos como modelos de "
"\"104 teclas\", que é provavelmente o que deseja se não souber qual o "
"teclado que tem.\n"

#. i18n: ectx: attribute (title), widget (QWidget, tabLayouts)
#: kcm_keyboard.ui:94
#, kde-format
msgid "Layouts"
msgstr "Disposições"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:102
#, kde-format
msgid ""
"If you select \"Application\" or \"Window\" switching policy, changing the "
"keyboard layout will only affect the current application or window."
msgstr ""
"Se seleccionar a política de mudança \"Aplicação\" ou \"Janela\", a mudança "
"de disposição do teclado só irá afectar a aplicação ou a janela actuais."

#. i18n: ectx: property (title), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:105
#, kde-format
msgid "Switching Policy"
msgstr "Política de Mudança"

#. i18n: ectx: property (text), widget (QRadioButton, switchByGlobalRadioBtn)
#: kcm_keyboard.ui:111
#, kde-format
msgid "&Global"
msgstr "&Global"

#. i18n: ectx: property (text), widget (QRadioButton, switchByDesktopRadioBtn)
#: kcm_keyboard.ui:124
#, kde-format
msgid "&Desktop"
msgstr "E&crã"

#. i18n: ectx: property (text), widget (QRadioButton, switchByApplicationRadioBtn)
#: kcm_keyboard.ui:134
#, kde-format
msgid "&Application"
msgstr "&Aplicação"

#. i18n: ectx: property (text), widget (QRadioButton, switchByWindowRadioBtn)
#: kcm_keyboard.ui:144
#, kde-format
msgid "&Window"
msgstr "&Janela"

#. i18n: ectx: property (title), widget (QGroupBox, shortcutsGroupBox)
#: kcm_keyboard.ui:157
#, kde-format
msgid "Shortcuts for Switching Layout"
msgstr "Atalhos para Mudar de Disposição"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm_keyboard.ui:163
#, kde-format
msgid "Main shortcuts:"
msgstr "Atalhos principais:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkbGrpShortcutBtn)
#: kcm_keyboard.ui:176
#, kde-format
msgid ""
"This is a shortcut for switching layouts which is handled by X.org. It "
"allows modifier-only shortcuts."
msgstr ""
"Este é um atalho para mudar de disposições, o qual será tratado pelo X.org. "
"Permite atalhos apenas com modificadores."

#. i18n: ectx: property (text), widget (QPushButton, xkbGrpShortcutBtn)
#. i18n: ectx: property (text), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:179 kcm_keyboard.ui:209
#, kde-format
msgctxt "no shortcut defined"
msgid "None"
msgstr "Nenhuma"

#. i18n: ectx: property (text), widget (QToolButton, xkbGrpClearBtn)
#. i18n: ectx: property (text), widget (QToolButton, xkb3rdLevelClearBtn)
#: kcm_keyboard.ui:186 kcm_keyboard.ui:216
#, kde-format
msgid "…"
msgstr "…"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm_keyboard.ui:193
#, kde-format
msgid "3rd level shortcuts:"
msgstr "Atalhos ao 3º nível:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:206
#, kde-format
msgid ""
"This is a shortcut for switching to a third level of the active layout (if "
"it has one) which is handled by X.org. It allows modifier-only shortcuts."
msgstr ""
"Este é um atalho para mudar para um terceiro nível da disposição activa (se "
"tiver uma), o qual será tratado pelo X.org. Permite atalhos apenas com "
"modificadores."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm_keyboard.ui:223
#, kde-format
msgid "Alternative shortcut:"
msgstr "Atalho alternativo:"

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, kdeKeySequence)
#: kcm_keyboard.ui:236
#, kde-format
msgid ""
"This is a shortcut for switching layouts. It does not support modifier-only "
"shortcuts and also may not work in some situations (e.g. if popup is active "
"or from screensaver)."
msgstr ""
"Este é um atalho para mudar de disposições. Não suporta atalhos apenas com "
"modificadores e também poderá não funcionar em algumas situações (p.ex., "
"numa janela activa ou no protector do ecrã)."

#. i18n: ectx: property (title), widget (QGroupBox, kcfg_configureLayouts)
#: kcm_keyboard.ui:261
#, kde-format
msgid "Configure layouts"
msgstr "Configurar as disposições"

#. i18n: ectx: property (text), widget (QPushButton, addLayoutBtn)
#: kcm_keyboard.ui:275
#, kde-format
msgid "Add"
msgstr "Adicionar"

#. i18n: ectx: property (text), widget (QPushButton, removeLayoutBtn)
#: kcm_keyboard.ui:285
#, kde-format
msgid "Remove"
msgstr "Remover"

#. i18n: ectx: property (text), widget (QPushButton, moveUpBtn)
#: kcm_keyboard.ui:295
#, kde-format
msgid "Move Up"
msgstr "Subir"

#. i18n: ectx: property (text), widget (QPushButton, moveDownBtn)
#: kcm_keyboard.ui:305
#, kde-format
msgid "Move Down"
msgstr "Descer"

#. i18n: ectx: property (text), widget (QCheckBox, layoutLoopingCheckBox)
#: kcm_keyboard.ui:350
#, kde-format
msgid "Spare layouts"
msgstr "Disposições extra"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm_keyboard.ui:382
#, kde-format
msgid "Main layout count:"
msgstr "Número de disposições principais:"

#. i18n: ectx: attribute (title), widget (QWidget, tabAdvanced)
#: kcm_keyboard.ui:412
#, kde-format
msgid "Advanced"
msgstr "Avançado"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_resetOldXkbOptions)
#: kcm_keyboard.ui:418
#, kde-format
msgid "&Configure keyboard options"
msgstr "&Configurar as opções do teclado"

#: kcm_keyboard_widget.cpp:211
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "Desconhecido"

#: kcm_keyboard_widget.cpp:213
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

#: kcm_keyboard_widget.cpp:643
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "Nenhuns"

#: kcm_keyboard_widget.cpp:657
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] "%1 atalho"
msgstr[1] "%1 atalhos"

#: kcm_view_models.cpp:200
#, kde-format
msgctxt "layout map name"
msgid "Map"
msgstr "Mapa"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Layout"
msgstr "Disposição"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Variant"
msgstr "Variante"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Label"
msgstr "Legenda"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Shortcut"
msgstr "Atalho"

#: kcm_view_models.cpp:273
#, kde-format
msgctxt "variant"
msgid "Default"
msgstr "Predefinição"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcmmiscwidget.ui:31
#, kde-format
msgid "When a key is held:"
msgstr "Quando uma tecla é mantida pressionada:"

#. i18n: ectx: property (text), widget (QRadioButton, accentMenuRadioButton)
#: kcmmiscwidget.ui:38
#, kde-format
msgid "&Show accented and similar characters "
msgstr "Mo&strar os caracteres acentuados e semelhantes"

#. i18n: ectx: property (text), widget (QRadioButton, repeatRadioButton)
#: kcmmiscwidget.ui:45
#, kde-format
msgid "&Repeat the key"
msgstr "&Repetir a tecla"

#. i18n: ectx: property (text), widget (QRadioButton, nothingRadioButton)
#: kcmmiscwidget.ui:52
#, kde-format
msgid "&Do nothing"
msgstr "&Não fazer nada"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcmmiscwidget.ui:66
#, kde-format
msgid "Test area:"
msgstr "Área de testes:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEdit)
#: kcmmiscwidget.ui:73
#, kde-format
msgid ""
"Allows to test keyboard repeat and click volume (just don't forget to apply "
"the changes)."
msgstr ""
"Permite testar a repetição e o volume do 'click' do teclado (não se esqueça "
"de aplicar as alterações)."

#. i18n: ectx: property (whatsThis), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:82
#, kde-format
msgid ""
"If supported, this option allows you to setup the state of NumLock after "
"Plasma startup.<p>You can configure NumLock to be turned on or off, or "
"configure Plasma not to set NumLock state."
msgstr ""
"Se for suportada, esta opção permite-lhe alterar o estado do NumLock depois "
"do arranque do Plasma.<p>Pode configurar o NumLock para estar ligado ou "
"desligado, ou ainda configurar o Plasma para não alterar o estado do NumLock."

#. i18n: ectx: property (title), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:85
#, kde-format
msgid "NumLock on Plasma Startup"
msgstr "NumLock no Arranque do Plasma"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton1)
#: kcmmiscwidget.ui:97
#, kde-format
msgid "T&urn on"
msgstr "&Ligar"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton2)
#: kcmmiscwidget.ui:104
#, kde-format
msgid "&Turn off"
msgstr "&Desligar"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton3)
#: kcmmiscwidget.ui:111
#, kde-format
msgid "Leave unchan&ged"
msgstr "Deixar &inalterado"

#. i18n: ectx: property (text), widget (QLabel, lblRate)
#: kcmmiscwidget.ui:148
#, kde-format
msgid "&Rate:"
msgstr "&Taxa:"

#. i18n: ectx: property (whatsThis), widget (QSlider, delaySlider)
#. i18n: ectx: property (whatsThis), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:164 kcmmiscwidget.ui:202
#, kde-format
msgid ""
"If supported, this option allows you to set the delay after which a pressed "
"key will start generating keycodes. The 'Repeat rate' option controls the "
"frequency of these keycodes."
msgstr ""
"Se for suportada, esta opção permite-lhe alterar o atraso ao fim do qual uma "
"tecla pressionada irá começar a gerar os códigos de teclas. A opção de 'Taxa "
"de repetição' controla a frequência desses códigos de teclas."

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, kcfg_repeatRate)
#. i18n: ectx: property (whatsThis), widget (QSlider, rateSlider)
#: kcmmiscwidget.ui:192 kcmmiscwidget.ui:212
#, kde-format
msgid ""
"If supported, this option allows you to set the rate at which keycodes are "
"generated while a key is pressed."
msgstr ""
"Se for suportada, esta opção permite-lhe alterar a taxa à qual os códigos de "
"teclas são gerados enquanto é carregada uma tecla."

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_repeatRate)
#: kcmmiscwidget.ui:195
#, kde-format
msgid " repeats/s"
msgstr " repetições/s"

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:205
#, kde-format
msgid " ms"
msgstr " ms"

#. i18n: ectx: property (text), widget (QLabel, lblDelay)
#: kcmmiscwidget.ui:246
#, kde-format
msgid "&Delay:"
msgstr "&Atraso:"

#: tastenbrett/main.cpp:57
#, kde-format
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Antevisão do Teclado"

#: tastenbrett/main.cpp:59
#, kde-format
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "Visualização da disposição do teclado"

#: tastenbrett/main.cpp:144
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant."
msgstr ""
"Não foi possível carregar o desenho geométrico do teclado. Isto normalmente "
"indica que o modelo seleccionado não suporta uma dada disposição ou variante "
"de disposição do teclado. Este problema também poderá acontecer quando "
"tentar usar esta combinação de modelo, disposição e variante."

#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "Selector de Disposições do Teclado do KDE"

#~ msgid "Only up to %1 keyboard layout is supported"
#~ msgid_plural "Only up to %1 keyboard layouts are supported"
#~ msgstr[0] "Só é suportada até %1 disposição de teclado"
#~ msgstr[1] "Só são suportadas até %1 disposições de teclado"

#~ msgid "Any language"
#~ msgstr "Qualquer língua"

#~ msgid "Layout:"
#~ msgstr "Disposição:"

#~ msgid "Variant:"
#~ msgstr "Variante:"

#~ msgid "Limit selection by language:"
#~ msgstr "Limitar a selecção por língua:"

#~ msgid "..."
#~ msgstr "..."

#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 - %2"

#~ msgid "Layout Indicator"
#~ msgstr "Indicador da Disposição"

#~ msgid "Show layout indicator"
#~ msgstr "Mostrar o indicador da disposição"

#~ msgid "Show for single layout"
#~ msgstr "Mostrar para uma única disposição"

#~ msgid "Show flag"
#~ msgstr "Mostrar a bandeira"

#~ msgid "Show label"
#~ msgstr "Mostrar a legenda"

#~ msgid "Show label on flag"
#~ msgstr "Mostrar o texto na bandeira"

#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "Disposição do Teclado"

#~ msgid "Configure Layouts..."
#~ msgstr "Configurar as Disposições..."
