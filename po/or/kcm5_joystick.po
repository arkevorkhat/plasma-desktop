# translation of joystick.po to Oriya
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Manoj Kumar Giri <mgiri@redhat.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: joystick\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-05 00:59+0000\n"
"PO-Revision-Date: 2008-12-23 14:10+0530\n"
"Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>\n"
"Language-Team: Oriya <oriya-it@googlegroups.com>\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ମନୋଜ କୁମାର ଗିରି"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mgiri@redhat.com"

#: caldialog.cpp:26 joywidget.cpp:339
#, kde-format
msgid "Calibration"
msgstr "ମାପ"

#: caldialog.cpp:40
#, kde-format
msgid "Next"
msgstr "ପରବର୍ତ୍ତି"

#: caldialog.cpp:50
#, kde-format
msgid "Please wait a moment to calculate the precision"
msgstr "ଯଥାର୍ଥତା ହିସାବ କରିବା ପାଇଁ ଦୟାକରି କିଛିକ୍ଷଣ ଅପେକ୍ଷା କରନ୍ତୁ"

#: caldialog.cpp:79
#, kde-format
msgid "(usually X)"
msgstr "(ସାଧାରଣତଃ X)"

#: caldialog.cpp:81
#, kde-format
msgid "(usually Y)"
msgstr "(ସାଧାରଣତଃ Y)"

#: caldialog.cpp:87
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>minimum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>ମାପ ହେଉଛି ଆପଣଙ୍କର ଉପକରଣର ଉତ୍ପାଦ ମୂଲ୍ୟକୁ ଯାଞ୍ଚ କରିବା।<br /><br />ଦୟାକରି <b>ଅକ୍ଷ %1 "
"%2</b> କୁ ଆପଣଙ୍କ ଉପକରଣରେ <b>ସର୍ବନିମ୍ନ</b> ସ୍ଥାନକୁ ଗତିକରନ୍ତୁ।<br /><br />ସେହି ଉପକରଣରେ "
"ଯେକୌଣସି ବଟନକୁ ଦବାନ୍ତୁ କିମ୍ବା ପରବର୍ତ୍ତି ପଦକ୍ଷେପ ସହିତ ଆଗକୁ ବଢ଼ିବା ପାଇଁ 'ପରବର୍ତ୍ତି' ବଟନକୁ ଦବାନ୍ତୁ।</"
"qt>"

#: caldialog.cpp:110
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>center</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>ମାପ ହେଉଛି ଆପଣଙ୍କର ଉପକରଣର ଉତ୍ପାଦ ମୂଲ୍ୟକୁ ଯାଞ୍ଚ କରିବା।<br /><br />ଦୟାକରି <b>ଅକ୍ଷ %1 "
"%2</b> କୁ ଆପଣଙ୍କ ଉପକରଣରେ <b>ମଧ୍ଯବର୍ତ୍ତି</b> ସ୍ଥାନକୁ ଗତିକରନ୍ତୁ।<br /><br />ସେହି ଉପକରଣରେ "
"ଯେକୌଣସି ବଟନକୁ ଦବାନ୍ତୁ କିମ୍ବା ପରବର୍ତ୍ତି ପଦକ୍ଷେପ ସହିତ ଆଗକୁ ବଢ଼ିବା ପାଇଁ 'ପରବର୍ତ୍ତି' ବଟନକୁ ଦବାନ୍ତୁ।</"
"qt>"

#: caldialog.cpp:133
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>maximum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>ମାପ ହେଉଛି ଆପଣଙ୍କର ଉପକରଣର ଉତ୍ପାଦ ମୂଲ୍ୟକୁ ଯାଞ୍ଚ କରିବା।<br /><br />ଦୟାକରି <b>ଅକ୍ଷ %1 "
"%2</b> କୁ ଆପଣଙ୍କ ଉପକରଣରେ <b>ସର୍ବାଧିକ</b> ସ୍ଥାନକୁ ଗତିକରନ୍ତୁ।<br /><br />ସେହି ଉପକରଣରେ "
"ଯେକୌଣସି ବଟନକୁ ଦବାନ୍ତୁ କିମ୍ବା ପରବର୍ତ୍ତି ପଦକ୍ଷେପ ସହିତ ଆଗକୁ ବଢ଼ିବା ପାଇଁ 'ପରବର୍ତ୍ତି' ବଟନକୁ ଦବାନ୍ତୁ।</"
"qt>"

#: caldialog.cpp:160 joywidget.cpp:329 joywidget.cpp:365
#, kde-format
msgid "Communication Error"
msgstr "ସଞ୍ଚାର-ବ୍ୟବସ୍ଥା ତ୍ରୁଟି"

#: caldialog.cpp:164
#, kde-format
msgid "You have successfully calibrated your device"
msgstr "ଆପଣ ସଫଳତାର ସହିତ ଆପଣଙ୍କର ଉପକରଣକୁ ମାପି ସାରିଛନ୍ତି"

#: caldialog.cpp:164 joywidget.cpp:367
#, kde-format
msgid "Calibration Success"
msgstr "ମାପ ସଫଳତା"

#: caldialog.cpp:184
#, kde-format
msgid "Value Axis %1: %2"
msgstr "ମୂଲ୍ୟ ଅକ୍ଷ %1: %2"

#: joydevice.cpp:41
#, kde-format
msgid "The given device %1 could not be opened: %2"
msgstr "ପ୍ରଦତ୍ତ ଉପକରଣ %1 କୁ ଖୋଲାଯାଇ ପାରୁନାହିଁ: %2"

#: joydevice.cpp:45
#, kde-format
msgid "The given device %1 is not a joystick."
msgstr "ପ୍ରଦତ୍ତ ଉପକରଣ %1 ଟି ଗୋଟିଏ joystick ନୁହଁ।"

#: joydevice.cpp:49
#, kde-format
msgid "Could not get kernel driver version for joystick device %1: %2"
msgstr "joystick ଉପକରଣ %1: %2 ପାଇଁ କର୍ଣ୍ଣଲ ଡ୍ରାଇଭର ସଂସ୍କରଣ ପାଇଲା ନାହିଁ।"

#: joydevice.cpp:60
#, kde-format
msgid ""
"The current running kernel driver version (%1.%2.%3) is not the one this "
"module was compiled for (%4.%5.%6)."
msgstr ""
"ବର୍ତ୍ତମାନ ଚାଲୁଥିବା କର୍ଣ୍ଣଲ ଡ୍ରାଇଭର ସଂସ୍କରଣ (%1.%2.%3) ଟି ଏହି ଏକକାଂଶ ସଂକଳନ କରିଥିବା ଏକକାଂଶ "
"ନୁହଁ (%4.%5.%6)।"

#: joydevice.cpp:71
#, kde-format
msgid "Could not get number of buttons for joystick device %1: %2"
msgstr "joystick ଉପକରଣ %1 ପାଇଁ ବଟନ ସଂଖ୍ୟା ପାଇଲା ନାହିଁ: %2"

#: joydevice.cpp:75
#, kde-format
msgid "Could not get number of axes for joystick device %1: %2"
msgstr "joystick ଉପକରଣ %1 ପାଇଁ ଅକ୍ଷଗୁଡ଼ିକର ସଂଖ୍ୟା ପାଇଲା ନାହିଁ: %2"

#: joydevice.cpp:79
#, kde-format
msgid "Could not get calibration values for joystick device %1: %2"
msgstr "joystick ଉପକରଣ %1 ପାଇଁ ମାପ ପାଇଲା ନାହିଁ: %2"

#: joydevice.cpp:83
#, kde-format
msgid "Could not restore calibration values for joystick device %1: %2"
msgstr "joystick ଉପକରଣ %1 ପାଇଁ ମାପକୁ ପୁନଃ ସ୍ଥାପନ କରିପାରିଲା ନାହିଁ: %2"

#: joydevice.cpp:87
#, kde-format
msgid "Could not initialize calibration values for joystick device %1: %2"
msgstr "joystick ଉପକରଣ %1 ପାଇଁ ମାପକୁ ଆରମ୍ଭ କରିପାରିଲା ନାହିଁ: %2"

#: joydevice.cpp:91
#, kde-format
msgid "Could not apply calibration values for joystick device %1: %2"
msgstr "joystick ଉପକରଣ %1 ପାଇଁ ମାପକୁ ପ୍ରୟୋଗ କରିପାରିଲା ନାହିଁ: %2"

#: joydevice.cpp:95
#, kde-format
msgid "internal error - code %1 unknown"
msgstr "ଆଭ୍ୟନ୍ତରୀଣ ତ୍ରୁଟି - ସଂକେତ %1 ଜଣାନାହିଁ"

#: joystick.cpp:29
#, kde-format
msgid "KDE Joystick Control Module"
msgstr "KDE Joystick ନିୟନ୍ତଣ ଏକକାଂଶ"

#: joystick.cpp:31
#, fuzzy, kde-format
#| msgid "KDE Control Center Module to test Joysticks"
msgid "KDE System Settings Module to test Joysticks"
msgstr "Joysticksକୁ ପରୀକ୍ଷା କରିବା ପାଇଁ KDE ନିୟନ୍ତଣ କେନ୍ଦ୍ର ଏକକାଂଶ"

#: joystick.cpp:33
#, kde-format
msgid "(c) 2004, Martin Koller"
msgstr "(c) 2004, Martin Koller"

#: joystick.cpp:38
#, kde-format
msgid ""
"<h1>Joystick</h1>This module helps to check if your joystick is working "
"correctly.<br />If it delivers wrong values for the axes, you can try to "
"solve this with the calibration.<br />This module tries to find all "
"available joystick devices by checking /dev/js[0-4] and /dev/input/"
"js[0-4]<br />If you have another device file, enter it in the combobox.<br /"
">The Buttons list shows the state of the buttons on your joystick, the Axes "
"list shows the current value for all axes.<br />NOTE: the current Linux "
"device driver (Kernel 2.4, 2.6) can only autodetect<ul><li>2-axis, 4-button "
"joystick</li><li>3-axis, 4-button joystick</li><li>4-axis, 4-button "
"joystick</li><li>Saitek Cyborg 'digital' joysticks</li></ul>(For details you "
"can check your Linux source/Documentation/input/joystick.txt)"
msgstr ""
"<h1>Joystick</h1>ଆପଣଙ୍କର joystick ସଠିକ ଭାବରେ କାର୍ଯ୍ୟ କରୁଛି କି ନାହିଁ ଯାଞ୍ଚ କରିବା ପାଇଁ ଏହି "
"ଏକକାଂଶଟି ସହାୟତା କରିଥାଏ।<br />ଅକ୍ଷଗୁଡ଼ିକ ପାଇଁ ଯଦି ଏହା ଭୁଲ ମୂଲ୍ୟ ପ୍ରଦାନ କରିଥାଏ, ମାପ ଦ୍ୱାରା "
"ଆପଣ ଏହାକୁ ସମାଧାନ କରିବା ପାଇଁ ଚେଷ୍ଟାକରିପାରିବେ।<br />ଏହି ଏକକାଂଶ /dev/js[0-4] ଏବଂ /dev/"
"input/js[0-4]କୁ ଯାଞ୍ଚ କରି ସମସ୍ତ ଉପଲବ୍ଧ joystick ଉପକରଣଗୁଡ଼ିକୁ ଖୋଜି ପାଇବାକୁ "
"ଚେଷ୍ଟାକରିଥାଏ<br />ଯଦି ଆପଣଙ୍କ ପାଖରେ ଅନ୍ୟ ଏକ ଉପକରଣ ଫାଇଲ ଅଛି, କମ୍ବୋବକ୍ସରେ ତାହାକୁ ଭରଣ "
"କରନ୍ତୁ।<br />ବଟନ ତାଲିକା ଆପଣଙ୍କ joystickରେ ଥିବା ସ୍ଥିତିକୁ ଦର୍ଶାଇଥାଏ, ଅକ୍ଷ ତାଲିକା ସମସ୍ତ "
"ଅକ୍ଷଗୁଡ଼ିକର ବର୍ତ୍ତମାନ ସ୍ଥିତିକୁ ଦର୍ଶାଇଥାଏ।<br />NOTE: ସାମ୍ପ୍ରତିକ Linux ଉପକରଣ ଡ୍ରାଇଭର (କର୍ଣ୍ଣଲ "
"2.4, 2.6) ହିଁ କେବଳ<ul><li>2-ଅକ୍ଷ, 4-ବଟନ joystick</li><li>3-ଅକ୍ଷ, 4-ବଟନ joystick</"
"li><li>4-ଅକ୍ଷ, 4-ବଟନ joystick</li><li>Saitek Cyborg 'ସାଂଖିକ' joysticks</li></"
"ul>କୁ ସ୍ୱୟଂଯାଞ୍ଚ କରିପାରିବ(ବିସ୍ତୃତ ବିବରଣୀ ପାଇଁ ଆପଣ ଆପଣଙ୍କ Linux ଉତ୍ସ/ଦଲିଲିକରଣ/ନିବେଶ/joystick."
"txtକୁ ଯାଞ୍ଚ କରିପାରିବେ)"

#: joywidget.cpp:67
#, kde-format
msgid "Device:"
msgstr "ଉପକରଣ:"

#: joywidget.cpp:84
#, kde-format
msgctxt "Cue for deflection of the stick"
msgid "Position:"
msgstr "ସ୍ଥିତି:"

#: joywidget.cpp:87
#, kde-format
msgid "Show trace"
msgstr "ଲକ୍ଷଣ ଦର୍ଶାନ୍ତୁ"

#: joywidget.cpp:96 joywidget.cpp:303
#, kde-format
msgid "PRESSED"
msgstr "PRESSED"

#: joywidget.cpp:98
#, kde-format
msgid "Buttons:"
msgstr "ବଟନଗୁଡ଼ିକ:"

#: joywidget.cpp:102
#, kde-format
msgid "State"
msgstr "ଅବସ୍ଥା"

#: joywidget.cpp:110
#, kde-format
msgid "Axes:"
msgstr "ଅକ୍ଷଗୁଡ଼ିକ:"

#: joywidget.cpp:114
#, kde-format
msgid "Value"
msgstr "ମୂଲ୍ୟ"

#: joywidget.cpp:127
#, kde-format
msgid "Calibrate"
msgstr "ମାପ"

#: joywidget.cpp:190
#, kde-format
msgid ""
"No joystick device automatically found on this computer.<br />Checks were "
"done in /dev/js[0-4] and /dev/input/js[0-4]<br />If you know that there is "
"one attached, please enter the correct device file."
msgstr ""
"ଏହି କମ୍ପୁଟରରେ ସ୍ୱୟଂଚାଳିତ ଭାବରେ କୌଣସି joystick ଉପକରଣ ମିଳିନାହିଁ।<br />/dev/js[0-4] ଏବଂ /"
"dev/input/js[0-4]ରେ ଯାଞ୍ଚ ସମ୍ପୂର୍ଣ୍ଣ ହୋଇଛି<br />ଯଦି ଆପଣ ଜାଣିଛନ୍ତି ଯେ ସେଠାରେ ଗୋଟିଏ ସଂଲଗ୍ନ "
"ଅଛି, ଦୟାକରି ସଠିକ ଉପକରଣ ଫାଇଲ ଭରଣ କରନ୍ତୁ।"

#: joywidget.cpp:226
#, kde-format
msgid ""
"The given device name is invalid (does not contain /dev).\n"
"Please select a device from the list or\n"
"enter a device file, like /dev/js0."
msgstr ""
"ପ୍ରଦତ୍ତ ଉପକରଣ ନାମଟି ଅବୈଧ ଅଟେ (/dev ଧାରଣ କରିନଥାଏ)।\n"
"ତାଲିକାରୁ ଗୋଟିଏ ଉପକରଣ ଚୟନ କରନ୍ତୁ କିମ୍ବା\n"
"ଗୋଟିଏ ଉପକରଣ ଫାଇଲ ଭରଣ କରନ୍ତୁ, ଯେପରିକି /dev/js0."

#: joywidget.cpp:229
#, kde-format
msgid "Unknown Device"
msgstr "ଅଜଣା ଉପକରଣ"

#: joywidget.cpp:247
#, kde-format
msgid "Device Error"
msgstr "ଉପକରଣ ତ୍ରୁଟି"

#: joywidget.cpp:265
#, kde-format
msgid "1(x)"
msgstr "1(x)"

#: joywidget.cpp:266
#, kde-format
msgid "2(y)"
msgstr "2(y)"

#: joywidget.cpp:335
#, kde-format
msgid ""
"<qt>Calibration is about to check the precision.<br /><br /><b>Please move "
"all axes to their center position and then do not touch the joystick anymore."
"</b><br /><br />Click OK to start the calibration.</qt>"
msgstr ""
"<qt>ଯଥାର୍ଥତା ଯାଞ୍ଚ କରିବା ପାଇଁ ମାପ ଥାଏ।<br /><br /><b>ଦୟାକରି ସମସ୍ତ ଅକ୍ଷଗୁଡ଼ିକୁ ସେମାନଙ୍କର "
"ମଧ୍ଯବର୍ତ୍ତି ସ୍ଥାନକୁ ଗତିକରାନ୍ତୁ ଏବଂ ତାପରେ joystickକୁ ଆଉ କେବେ ଛୁଅନ୍ତୁ ନାହିଁ। </b><br /><br /"
">ମାପ ଆରମ୍ଭ କରିବା ପାଇଁ ଠିକ ଅଛିକୁ ଦବାନ୍ତୁ।</qt>"

#: joywidget.cpp:367
#, kde-format
msgid "Restored all calibration values for joystick device %1."
msgstr "joystick ଉପକରଣ %1 ପାଇଁ ସମସ୍ତ ମପାଯାଇଥିବା ମୂଲ୍ୟକୁ ପୁନଃ ସ୍ଥାପନ କରନ୍ତୁ।"
