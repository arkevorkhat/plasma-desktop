# Frederik Schwarzer <schwarzer@kde.org>, 2014, 2016, 2022.
# Burkhard Lück <lueck@hube-lueck.de>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-29 00:59+0000\n"
"PO-Revision-Date: 2022-07-23 23:50+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.07.70\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Allgemein"

#: contents/ui/ConfigGeneral.qml:27
#, kde-format
msgid "Show active application's name on Panel button"
msgstr ""

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Only icons can be shown when the Panel is vertical."
msgstr ""

#: contents/ui/ConfigGeneral.qml:49
#, kde-format
msgid "Not applicable when the widget is on the Desktop."
msgstr ""

#: contents/ui/main.qml:86
#, kde-format
msgid "Plasma Desktop"
msgstr "Plasma-Arbeitsfläche"

#~ msgid "Show list of opened windows"
#~ msgstr "Liste aller geöffneten Fenster anzeigen"

#~ msgid "On all desktops"
#~ msgstr "Auf allen Arbeitsflächen"

#~ msgid "Window List"
#~ msgstr "Fensterliste"

#~ msgid "Actions"
#~ msgstr "Aktionen"

#~ msgid "Unclutter Windows"
#~ msgstr "Fenster staffeln"

#~ msgid "Cascade Windows"
#~ msgstr "Fenster anordnen"

#~ msgctxt "%1 is the name of the desktop"
#~ msgid "Desktop %1"
#~ msgstr "Arbeitsfläche %1"
