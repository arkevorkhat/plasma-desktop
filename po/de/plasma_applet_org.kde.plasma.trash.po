# Frederik Schwarzer <schwarzer@kde.org>, 2014, 2016.
# Burkhard Lück <lueck@hube-lueck.de>, 2014, 2015, 2016, 2017, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2021-05-26 06:35+0200\n"
"Last-Translator: Burkhard Lück <lueck@hube-lueck.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/ui/main.qml:100
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "Öffnen"

#: contents/ui/main.qml:101
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "Leeren"

#: contents/ui/main.qml:107
#, kde-format
msgid "Trash Settings…"
msgstr "Papierkorb-Einstellungen ..."

#: contents/ui/main.qml:158
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"Papierkorb\n"
"Leer"

#: contents/ui/main.qml:158
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"Papierkorb\n"
"Ein Objekt"
msgstr[1] ""
"Papierkorb\n"
"%1 Objekte"

#: contents/ui/main.qml:167
#, kde-format
msgid "Trash"
msgstr "Papierkorb"

#: contents/ui/main.qml:168
#, kde-format
msgid "Empty"
msgstr "Leer"

#: contents/ui/main.qml:168
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "Ein Eintrag"
msgstr[1] "%1 Einträge"

#~ msgid ""
#~ "Trash \n"
#~ " Empty"
#~ msgstr ""
#~ "Papierkorb\n"
#~ "Leer"

#~ msgid "Empty Trash"
#~ msgstr "Papierkorb leeren"

#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr ""
#~ "Soll der Papierkorb wirklich geleert werden? Der gesamte Inhalt wird "
#~ "gelöscht."

#~ msgid "Cancel"
#~ msgstr "Abbrechen"
