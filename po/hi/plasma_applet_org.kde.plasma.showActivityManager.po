# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Raghavendra Kamath <raghu@raghukamath.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2021-10-11 11:50+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: kde-hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 21.08.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "दिखावट"

#: package/contents/ui/ConfigAppearance.qml:24
#, kde-format
msgid "Icon:"
msgstr "प्रतीक :"

#: package/contents/ui/ConfigAppearance.qml:26
#, kde-format
msgid "Show the current activity icon"
msgstr "वर्तामान गतिविधि प्रतीक दिखाएँ"

#: package/contents/ui/ConfigAppearance.qml:32
#, kde-format
msgid "Show the generic activity icon"
msgstr "सामान्य गतिविधि प्रतीक दिखाएं"

#: package/contents/ui/ConfigAppearance.qml:42
#, kde-format
msgid "Title:"
msgstr "शीर्षक :"

#: package/contents/ui/ConfigAppearance.qml:44
#, kde-format
msgid "Show the current activity name"
msgstr "वर्तामान गतिविधि का नाम दिखाएँ"

#: package/contents/ui/main.qml:73
#, fuzzy, kde-format
#| msgid "Show the current activity icon"
msgctxt "@info:tooltip"
msgid "Current activity is %1"
msgstr "वर्तामान गतिविधि प्रतीक दिखाएँ"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Show Activity Manager"
msgstr "गतिविधि प्रबंधक दिखाएं"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Click to show the activity manager"
msgstr "गतिविधि प्रबंधक दिखाने के लिए क्लिक करें"
