# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2014, 2015, 2016, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2021-10-07 18:03-0700\n"
"Last-Translator: A S Alam <aalam@satluj.org>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#: contents/ui/main.qml:100
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "ਖੋਲ੍ਹੋ"

#: contents/ui/main.qml:101
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "ਖਾਲੀ"

#: contents/ui/main.qml:107
#, kde-format
msgid "Trash Settings…"
msgstr "…ਰੱਦੀ ਲਈ ਸੈਟਿੰਗਾਂ"

#: contents/ui/main.qml:158
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"ਰੱਦੀ\n"
"ਖਾਲੀ"

#: contents/ui/main.qml:158
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"ਇੱਕ ਆਈਟਮ\n"
"ਰੱਦੀ 'ਚ ਭੇਜੋ"
msgstr[1] ""
"%1 ਆਈਟਮਾਂ\n"
"ਰੱਦੀ ਵਿੱਚ ਭੇਜੋ"

#: contents/ui/main.qml:167
#, kde-format
msgid "Trash"
msgstr "ਰੱਦੀ"

#: contents/ui/main.qml:168
#, kde-format
msgid "Empty"
msgstr "ਖਾਲੀ ਕਰੋ"

#: contents/ui/main.qml:168
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "ਇੱਕ ਆਈਟਮ"
msgstr[1] "%1 ਆਈਟਮਾਂ"

#~ msgid ""
#~ "Trash \n"
#~ " Empty"
#~ msgstr ""
#~ "ਰੱਦੀ\n"
#~ "ਖਾਲੀ ਕਰੋ"

#~ msgid "Empty Trash"
#~ msgstr "ਰੱਦੀ ਖਾਲੀ ਕਰੋ"

#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr "ਕੀ ਤੁਸੀਂ ਰੱਦੀ ਖਾਲੀ ਕਰਨੀ ਚਾਹੁੰਦੇ ਹੋ? ਸਭ ਆਈਟਮਾਂ ਹਟਾਈਆਂ ਜਾਣਗੀਆਂ।"

#~ msgid "Cancel"
#~ msgstr "ਰੱਦ ਕਰੋ"
